﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;


namespace MercadIn.View
{
    public partial class AlteracaoCliente : MaterialSkin.Controls.MaterialForm // Atribuindo Design do MaterialSkin no form.
    {
        // Vars para receber do ConsultarCliente
        public String ID;
        public String CPF;
        public String CONTATO;
        public String NOME;
        public String EMAIL;
        public String DATA;
        
        public AlteracaoCliente(string id, string cpf, string contato, string nome, string email, string data)
        {          
            InitializeComponent();
            // Definindo design com bibliotécas do Material Skin
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            // Setando os valores nas variaveis 
            ID = id;
            CPF = cpf;
            CONTATO = contato;
            NOME = nome;
            EMAIL = email;
            DATA = data;
        }
        public void LimparCampos() // Método para limpar os campos
        {
            txtID.Text = "";
            maskedTextBox2.Text = "";
            maskedTextBox3.Text = "";
            txtNome.Text = "";
            txtEmail.Text = "";
            maskedTextBox1.Text = "";
        }

        private void AlteracaoRemocaoCliente_Load(object sender, EventArgs e)
        {
            // Atribuindo os valores que estão nas vars, para os campos de textos.
            txtID.Text = ID;
            maskedTextBox2.Text = CPF;
            maskedTextBox3.Text = CONTATO;
            txtNome.Text = NOME;
            txtEmail.Text = EMAIL;
            maskedTextBox1.Text = DATA;
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            // Evento de fechar tela.
            this.Close();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            // Mandando os valores para ClienteController, no método CrudCliente caso de certo na alteração ira aparecer um
            // MessageBox, depois limpar os campos, fechar a tela e abrir o ConsultarCliente
            // Se não aparece um MessageBox com erro.
            if (new ClienteController().CrudCliente(int.Parse(txtID.Text), maskedTextBox2.Text, maskedTextBox3.Text, txtNome.Text, txtEmail.Text, maskedTextBox1.Text))
            {
                MessageBox.Show("Cliente alterado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LimparCampos();
                Hide();
                ConsultarCliente cc = new ConsultarCliente(1);
                cc.ShowDialog();
            }
            else
            {
                MessageBox.Show("Erro ao alterar o cliente!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimparCampos(); // Puxando método para limpar os campos
        }

        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }

        private void maskedTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }

        private void maskedTextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }
    }
}
