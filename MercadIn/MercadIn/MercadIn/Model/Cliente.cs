﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using MercadIn.Conexao;

namespace MercadIn.Model

{
    class Cliente
    {
          public int id { get; set; }
          public string cpf { get; set; }
          public string contato { get; set; }
          public string nome { get; set; }
          public string email { get; set; }
          public string senha { get; set; }
          public string dataNascimento { get; set; }


         public bool Crud(Cliente obj)
        {
            string strSQL;
            if (obj.id == 0)
            {               
                strSQL = "insert into vini.Cliente (cpf, contato, nome, email, dataNascimento, senha) values (";
                strSQL += "'" + obj.cpf + "',";
                strSQL += "'" + obj.contato + "',";
                strSQL += "'" + obj.nome + "',";
                strSQL += "'" + obj.email + "',";
                strSQL += "'" + obj.dataNascimento + "', '123')";
                return new DB().Insert(strSQL);
            }
            else
            {
                strSQL = "update vini.Cliente set cpf = '" + obj.cpf + "',contato = '" + obj.contato + "',nome = '" + obj.nome +
                    "',email = '" + obj.email + "',dataNascimento = '" + obj.dataNascimento + "' where id = "+ obj.id;
                return new DB().Update(strSQL);
            }

        }

        public DataTable ListarComprasQuitacoes(int id)
        {
            return new DB().Select("SELECT DISTINCT C.nome Cliente, (SELECT DISTINCT max(dataCompra) FROM vini.Compra WHERE id = CO.id) dataCompra, (SELECT SUM(valorCompra) FROM vini.Compra WHERE id = CO.id) valorCompra, TP.descricao FormaDePagamento FROM vini.Cliente C INNER JOIN vini.Compra CO ON CO.idCliente = C.id INNER JOIN vini.Pagamento P ON P.idCompra = CO.id INNER JOIN vini.TipoPagamento TP ON TP.id = P.idTipoPagamento WHERE C.id = "+ id + " ORDER BY CO.dataCompra DESC");
        }
        public DataTable ListarClientes()
        {           
           return new DB().Select("select * from vini.Cliente");
        }
        public bool Deletar(int id)
        {
            return new DB().Delete("delete from vini.Cliente WHERE id = " + id);
        }

        public DataTable ListarNomeCliente()
        {
            return new DB().Select("select id,nome from vini.Cliente");
        }

        public int SelecionarCliente(string cpf)
        {
            return new DB().SelecionarCliente(cpf);
        }
        public int BuscarCliente(string usuario, string senha)
        {
            int resultado = 0;
            DataTable dt = new DB().Select("SELECT id FROM vini.Cliente WHERE email = '" + usuario + "' AND senha = '" + senha + "'");
            try
            {
                foreach (DataRow linha in dt.Rows)
                    foreach (object obj in linha.ItemArray)
                        resultado = Convert.ToInt32(obj.ToString());
            }
            catch (Exception ex)
            {
                resultado = 0;
            }
            return resultado;
        }
    }
}
