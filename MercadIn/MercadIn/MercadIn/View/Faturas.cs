﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MercadIn.Model;
using DGVPrinterHelper;
using MercadIn.Controller;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using DGVPrinterHelper;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class Faturas : MaterialSkin.Controls.MaterialForm
    {
        string ID;
        public Faturas(string id)
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            ID = id;
        }
        public void Atualizar()
        {
            new FaturaController().ListarFaturas(dgvFaturas, Convert.ToInt32(ID));
        }
        private void FaturasEmAtraso_Load(object sender, EventArgs e)
        {
            Atualizar();
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirmação de pagamento de fatura!!?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                if (new FaturaController().Pagar(int.Parse(dgvFaturas.CurrentRow.Cells[0].Value.ToString())))
                {
                    MessageBox.Show("Fatura paga com sucesso!!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Atualizar();
                }
            }
        }
    }
}
