﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MercadIn.Controller;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using DGVPrinterHelper;

namespace MercadIn.View
{
    public partial class EstoqueDeProdutos : MaterialSkin.Controls.MaterialForm
    {
        public EstoqueDeProdutos()
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
        }
        public void minimizarTELA()
        {
            this.Hide();           
        }
        public void Atualizar()
        {
            new ProdutoController().ListarEstoque(dgvQuantidade);
        }
        private void EstoqueProdutos_Load(object sender, EventArgs e)
        {
            Atualizar();
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            DGVPrinter print = new DGVPrinter();
            print.Title = "ESTOQUE DE PRODUTOS\n\n\n";
            print.SubTitleAlignment = StringAlignment.Near;
            print.SubTitle = string.Format("DATA: {0}", DateTime.Now.Date.ToString("dd/MM/yyyy"));
            print.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
            print.PageNumbers = true;
            print.PageNumberInHeader = false;
            print.PorportionalColumns = true;
            print.HeaderCellAlignment = StringAlignment.Center;
            print.Footer = "Mercadin";
            print.FooterSpacing = 15;
            print.PrintDataGridView(dgvQuantidade);
            minimizarTELA();
        }
        
    }
}
