﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MercadIn.Model;
using MercadIn.View;

namespace MercadIn.Controller
{
    class CaixaController
    {
        public bool CrudCaixa(int id,int idProduto, int idServico, string dataCompra, decimal valorCompra, string Cpf)
        {
            Caixaa cx = new Caixaa();
            cx.id = id;
            cx.idServico = idServico;
            cx.idProduto = idProduto;
            cx.dataCompra = dataCompra;
            cx.valorCompra = valorCompra;
            cx.idCliente = new Cliente().SelecionarCliente(Cpf);
            return new Caixaa().CrudCaixa(cx);
        }
        public bool CrudPagamento(int id, int idCompra, decimal valor, int idTipoPagamento)
        {
            Pagamento pg = new Pagamento();
            pg.id = id;
            pg.idCompra = idCompra;
            pg.valor = valor;
            pg.idTipoPagamento = idTipoPagamento;
            return new Pagamento().CrudPagamento(pg);
        }
        public void Fatura(decimal valorTotal, int data)
        {
            int caixa = new Conexao.DB().PuxarIdPagamento();
            string strMySql = "insert into vini.Fatura (idPagamento, valorTotalPago, dataVencimento) values (" + caixa + ", '" + valorTotal.ToString().Replace(",", ".") + "', + date_add(current_date(), interval + " + data + " day))";
            new Conexao.DB().Insert(strMySql);
        }
    }
}
