﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class CadastroDeProdutos : MaterialSkin.Controls.MaterialForm //instancia do design do form do materialskin.
    {
        Model.Cliente modd = new Model.Cliente();
        public CadastroDeProdutos()
        {
            InitializeComponent();
            // MaterialSkin bibliotecas para setar o design no form.
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
        }


        private void CadastroDeProdutos_Load(object sender, EventArgs e)
        {
            //Puxando os nome das empresas e setando os id nos valores no cbxEmpresa "ComboBox" para poder tratar-los na view.
            cbxEmpresa.DataSource = new Empresa().ListarEmpresas();
            cbxEmpresa.DisplayMember = "Nome";
            cbxEmpresa.ValueMember = "id";
            cbxEmpresa.SelectedIndex = -1;
            //Puxando os nome das categorias e setando os id nos valores no cbxCategoria "ComboBox" para poder tratar-los na view.
            cbxCategoria.DataSource = new Categoria().ListarCategoria();
            cbxCategoria.DisplayMember = "Nome";
            cbxCategoria.ValueMember = "id";
            cbxCategoria.SelectedIndex = -1;
        } 
        
        public void LimparCampos() //Método Limpar os Campos .
        {
            txtCodigoBarras.Text = "";
            txtQuantidadeEstoque.Text = "";
            txtNome.Text = "";
            txtCompra.Text = "";
            txtVenda.Text = "";
            txtQuantidadeEstoque.Text = "";
            maskedTextBox1.Text = "";
            cbxCategoria.SelectedIndex = -1;
            cbxEmpresa.SelectedIndex = -1;
        }
        private void btnLimpar_Click_1(object sender, EventArgs e)
        {
            LimparCampos(); // Puxando o método de limpar os campos.
        }

        private void btnVoltar_Click_1(object sender, EventArgs e)
        {
            this.Close(); // Fechando a view.
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            // Verificação se todos os campos estiverem com dados setados para poder continuar se os campos não estiverem completos ira.
            // Exibir uma mensagem "Preencha os campos obrigatórios ou digar corretamente nos campos!".
            if (txtNome.Text != "" && cbxEmpresa.SelectedIndex != -1 && 
                cbxCategoria.SelectedIndex != -1 && txtCompra.Text != "" && txtVenda.Text != ""
                && txtQuantidadeEstoque.Text != "")
            {
                // Enviando todos os dados necessários para o ProdutoController para executar o método ProdutoCrud se estiver tudo ok ele exibe uma
                // MessageBox "Produto adicionado com sucesso", e limpando os campos, senão ele exibe uma messagem "Erro ao adicionar o Produto ".     
                if (new ProdutoController().ProdutoCrud(0, maskedTextBox1.Text, int.Parse(txtQuantidadeEstoque.Text), decimal.Parse(txtVenda.Text.Replace(".",",")), decimal.Parse(txtCompra.Text.Replace(".", ",")), txtNome.Text, txtCodigoBarras.Text, int.Parse(cbxCategoria.SelectedValue.ToString()), int.Parse(cbxEmpresa.SelectedValue.ToString())))
                {
                    MessageBox.Show("Produto adicionado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LimparCampos();
                }
                else
                {
                    MessageBox.Show("Erro ao adicionar o Produto", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Preencha os campos obrigatórios ou digar corretamente nos campos!", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCompra_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 46) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }

        private void txtVenda_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 46) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }

        private void txtQuantidadeEstoque_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }
    }
}
