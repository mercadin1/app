﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class ConsultarCategoria : MaterialSkin.Controls.MaterialForm
    {
        public int I;
        public ConsultarCategoria(int i)
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            I = i;
            if (I == 0)
            {
                btnAlterar.Enabled = false;
                btnExcluir.Enabled = false;
            }
        }
        

        public void Atualizar()
        {
            new Controller.CategoriaController().Listar(dgvCategoria);
        }
        private void ConsultaCategoria_Load(object sender, EventArgs e)
        {
            Atualizar();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Hide();
            AlteracaoCategoria alteracaoRemocaoCategoria = new AlteracaoCategoria(dgvCategoria.CurrentRow.Cells[0].Value.ToString(), dgvCategoria.CurrentRow.Cells[1].Value.ToString());
            alteracaoRemocaoCategoria.ShowDialog();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja excluir a Categoria?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                if (new CategoriaController().Deletar(int.Parse(dgvCategoria.CurrentRow.Cells[0].Value.ToString())))
                {
                    MessageBox.Show("Categoria excluido com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Atualizar();
                }
            }
        }
    }
}
