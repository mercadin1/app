﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class CadastroDeCategoria : MaterialSkin.Controls.MaterialForm //instancia do design do form do materialskin.
    {
        public CadastroDeCategoria()
        {
            InitializeComponent();
            // MaterialSkin bibliotecas para setar o design no form.
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
        }

        private void CadastroDeCategoria_Load(object sender, EventArgs e)
        {

        }

        private void materialFlatButton2_Click(object sender, EventArgs e)
        {
            txtNome.Text = ""; // Limpando o campo de texto
        }

        private void materialFlatButton3_Click(object sender, EventArgs e)
        {
            // Evento de fechar tela.
            this.Close(); 
        }

        private void btnCadastrarCliente_Click(object sender, EventArgs e)
        {
            // Verifica se o campo de texto tem alguma coisa se tiver, ele manda o valor para CategoriaController,
            // No método CrudCategoria caso de certo na inserção de dados, ira aparecer um MessageBox depois limpa o campo de texto
            // Se não aparece um MessageBox de erro.
            if (txtNome.Text != "")
            {
                if (new CategoriaController().CrudCategoria(0, txtNome.Text))
                {
                    MessageBox.Show("Categoria adicionado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Text = "";
                }
                else
                {
                    MessageBox.Show("Erro ao adicionar a categoria", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Preencha o campo obrigatório!", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    } 
}

