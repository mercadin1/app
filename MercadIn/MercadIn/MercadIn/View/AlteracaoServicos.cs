﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class AlteracaoServicos : MaterialSkin.Controls.MaterialForm // Atribuindo Design do MaterialSkin no form.
    {
        // Vars para receber do ConsultarServico
        public String NOME;
        public String ID;
        public String DESCRICAO;
        public decimal PRECO;

        public AlteracaoServicos(string nome, string id, string descricao, decimal preco)
        {
            InitializeComponent();
            // Definindo design com bibliotécas do Material Skin
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            // Setando os valores nas variaveis 
            NOME = nome;
            ID = id;
            DESCRICAO = descricao;
            PRECO = preco;
        }

        private void AlteracaoRemocaoServicos_Load(object sender, EventArgs e)
        {
            // Atribuindo os valores que estão nas vars, para os campos de textos.
            txtID.Text = ID;
            txtNome.Text = NOME;
            txtPreco.Text = PRECO.ToString();
            txtDescricao.Text = DESCRICAO;
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close(); // Evento de fechar tela.
        }

        public void LimparCampos() // Método para limpar os campos
        {
            txtDescricao.Text = "";
            txtNome.Text = "";
            txtPreco.Text = "";
        }
        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimparCampos(); // Puxando método para limpar os campos
        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 46) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            // Mandando os valores para ServicoController, no método CrudServicos caso de certo na alteração ira aparecer um
            // MessageBox, depois limpar os campos , fechar a tela e abrir a ConsultarCategoria
            // Se não aparece um MessageBox, com erro.
            if (new ServicoController().CrudServicos(txtNome.Text, int.Parse(txtID.Text), txtDescricao.Text, decimal.Parse(txtPreco.Text)))
            {
                MessageBox.Show("Serviço alterado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LimparCampos();
                Hide();
                ConsultarServico servico = new ConsultarServico(1);
                servico.ShowDialog();
            }
            else
            {
                MessageBox.Show("Erro ao alterar o Serviço!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
