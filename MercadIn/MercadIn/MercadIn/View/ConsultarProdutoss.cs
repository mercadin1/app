﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class ConsultarProdutoss : MaterialSkin.Controls.MaterialForm
    {
        public int I;
        public ConsultarProdutoss(int i)
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            I = i;
            if (I == 0)
            {
                btnAlterar.Enabled = false;
                btnExcluir.Enabled = false;
            }
        }

        public void atualizar()
        {
            new Controller.ProdutoController().ListarProduto(dgvProdutos);
        }
        private void ConsultarProdutoss_Load(object sender, EventArgs e)
        {
            atualizar();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja excluir o Produto?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                if (new ProdutoController().Deletar(int.Parse(dgvProdutos.CurrentRow.Cells[0].Value.ToString())))
                {
                    MessageBox.Show("Produto excluido com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    atualizar();
                }
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            
            Hide();
            AlteracaoProdutos alteracaoProdutos = new AlteracaoProdutos(dgvProdutos.CurrentRow.Cells[0].Value.ToString(), dgvProdutos.CurrentRow.Cells[2].Value.ToString(),
                dgvProdutos.CurrentRow.Cells[3].Value.ToString(), dgvProdutos.CurrentRow.Cells[5].Value.ToString(), dgvProdutos.CurrentRow.Cells[4].Value.ToString(),
               dgvProdutos.CurrentRow.Cells[1].Value.ToString(), dgvProdutos.CurrentRow.Cells[6].Value.ToString());
            alteracaoProdutos.ShowDialog();
        }
    }
}
