﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MercadIn.Model;
using MercadIn.View;

namespace MercadIn.Controller
{
    class CategoriaController
    {
        public bool CrudCategoria (int id, string nome)
        {
            Categoria cate = new Categoria();
            cate.id = id;
            cate.nome = nome;
            return new Categoria().Crud(cate);
        }
        public void Listar(DataGridView objetoResposta)
        {
            objetoResposta.DataSource = new Categoria().ListarCategoria();
        }
        public bool Deletar(int id)
        {
            return new Categoria().deletar(id);
        }
    }
}
