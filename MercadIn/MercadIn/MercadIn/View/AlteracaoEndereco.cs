﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class AlteracaoEndereco : MaterialSkin.Controls.MaterialForm // Atribuindo Design do MaterialSkin no form.
    {
        // Vars para receber do ConsultarEndereco
        string CLIENTE;
        string CEP;
        string CIDADE;
        string ESTADO;
        string RUA;
        string COMPLEMENTO;
        string NOME;

        public AlteracaoEndereco(string id, string cep, string cidade, string estado, string rua, string complemento, string nome)
        {
            InitializeComponent();
            // Definindo design com bibliotécas do Material Skin
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            // Setando os valores nas variaveis 
            CLIENTE = id;
            CEP = cep;
            CIDADE = cidade;
            ESTADO = estado;
            RUA = rua;
            COMPLEMENTO = complemento;
            NOME = nome;
        }

        private void AlteracaoEndereco_Load(object sender, EventArgs e)
        {
            // Atribuindo os valores que estão nas vars, para os campos de textos.
            txtID.Text = CLIENTE;
            txtCep.Text = CEP;
            txtCidade.Text = CIDADE;
            txtEstado.Text = ESTADO;
            txtRua.Text = RUA;
            txtComplemento.Text = COMPLEMENTO;
            txtNome.Text = NOME;
        }

        public void LimparCampos() // Método para limpar os campos
        {
            txtCep.Text = "";
            txtCidade.Text = "";
            txtComplemento.Text = "";
            txtEstado.Text = "";
            txtNome.Text = "";
            txtRua.Text = "";
        }
        private void materialFlatButton2_Click(object sender, EventArgs e)
        {
            LimparCampos(); // Puxando método para limpar os campos
        }

        private void materialFlatButton3_Click(object sender, EventArgs e)
        {
            this.Close(); // Evento de fechar tela.
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            // Mandando os valores para EnderecoController, no método EnderecoCrud caso de certo na alteração ira aparecer um
            // MessageBox, depois limpar os campos, fechando a tela e abrindo ConsultaEnderecos
            // Se não aparece um messagebox com erro.
            if (new EnderecoController().EnderecoCrud(int.Parse(txtID.Text), int.Parse(txtNome.Text), txtCep.Text, txtCidade.Text, txtEstado.Text, txtRua.Text                ,txtComplemento.Text))
            {
                MessageBox.Show("Endereço alterado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LimparCampos();
                Hide();
                ConsultaEndereços consultaEndereços = new ConsultaEndereços(1);
                consultaEndereços.ShowDialog();
            }
            else
            {
                MessageBox.Show("Erro ao alterar o endereço!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
