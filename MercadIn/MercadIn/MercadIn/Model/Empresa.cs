﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MercadIn.Conexao;


namespace MercadIn.Model
{
    class Empresa
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string razaoSocial { get; set; }
        public string cnpj { get; set; }


        public bool CrudEmpresa(Empresa obj)
        {
            string strSQL;
            if (obj.id == 0)
            {
                strSQL = "insert into vini.Empresa (id, nome, razaoSocial, cnpj) values (";
                strSQL += obj.id + ",";
                strSQL += "'" + obj.nome + "',";
                strSQL += "'" + obj.razaoSocial + "',";
                strSQL += "'" + obj.cnpj + "')";
                return new DB().Insert(strSQL);
            }
            else
            {
                strSQL = "update vini.Empresa set nome = '" + obj.nome + "',razaoSocial = '" + obj.razaoSocial + "',cnpj= '" + obj.cnpj + "' where id = " + obj.id;
                return new DB().Update(strSQL);
            }

        }
            public DataTable ListarEmpresas()
        {
            return new DB().Select("select id, nome, razaoSocial, cnpj from vini.Empresa");
        }

             public bool Deletar(int id)
        {
            return new DB().Delete("delete from vini.Empresa where id= " + id);
        }
        }
    }

