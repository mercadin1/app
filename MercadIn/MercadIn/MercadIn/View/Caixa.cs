﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using MercadIn.Controller;
using MySql.Data.MySqlClient;
using MercadIn.Conexao;


namespace MercadIn.View
{
    public partial class Caixa : MaterialSkin.Controls.MaterialForm // Atribuindo Design do MaterialSkin no form.
    {
        // var para usar nos calculos de valores.
        float totalvenda = 0;
        public Caixa()
        {
            InitializeComponent();
            // Definindo design com bibliotécas do Material Skin
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
        }

        private void Caixa_Load(object sender, EventArgs e)
        {
            // Puxando método para saber qual codigo da ultima venda e gerando um novo para compra.
            GerarCodigoVenda();
            // Puxando método para nomear o datagridview dgvCaixa1.
            NomearDataGrid();
            // Puxando método para setar num datagridview os produtos e os id"
            SaberProdutos();
            // Puxando os nomes dos serviços e setando os id nos valores no cbxServicos para poder tratar-los na view.
            cbxServicos.DataSource = new Servicos().ListarServicos();
            cbxServicos.DisplayMember = "Nome";
            cbxServicos.ValueMember = "id";
            cbxServicos.SelectedIndex = -1;           

        }

        public void NomearDataGrid() // Método para nomear o datagridview.
        {
            dgvCaixa1.ColumnCount = 7;

            dgvCaixa1.Columns[0].Name = "Código Serviço";
            dgvCaixa1.Columns[1].Name = "Serviço";
            dgvCaixa1.Columns[2].Name = "Código Produto";
            dgvCaixa1.Columns[3].Name = "Produto";
            dgvCaixa1.Columns[4].Name = "Valor";
            dgvCaixa1.Columns[5].Name = "Quantidade";
            dgvCaixa1.Columns[6].Name = "Total";
            dgvCaixa1.Columns[3].Width = 150;
        }

        public void SaberProdutos() // Método para listar os produtos em um datagridview.
        {
            dgvProduto.DataSource = new Produto().ListarProduto();
        }
        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close(); // Fechando a view.
        }

        private void rbCredito_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cbxServicos_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConsultarServico();
        }

        public void ConsultarServico()
        {
            string idServicos = " " + cbxServicos.SelectedValue;
            Model.Servicos serv = new ServicoController().SelecionarServico(idServicos);
            if (serv != null)
            {
                txtCodProduto.Text = serv.id.ToString();
                txtNomeProduto.Text = serv.nome;
                txtValorProduto.Text = serv.preco.ToString();
                txtQuantidade.Text = "1";
                txtValorTotal.Text = serv.preco.ToString();
            }

        }
        public void ConsultarProduto()
        {
            string idProduto = " " + txtCodProduto.Text;
            Model.Produto prod = new ProdutoController().SelecionarProduto(idProduto);
            if (prod != null)
            {
                txtCodProduto.Text = prod.id.ToString();
                txtNomeProduto.Text = prod.nome;
                txtValorProduto.Text = prod.precoVenda.ToString(); ;

            }
        }

        private void txtCodProduto_Validating(object sender, CancelEventArgs e)
        {
            if (txtCodProduto.Text != string.Empty && cbxServicos.SelectedIndex == -1)
            {
                ConsultarProduto();
            }
            else
            {
                cbxServicos.Focus();

            }
        }

        private void txtQuantidade_Validating(object sender, CancelEventArgs e)
        {
            if (txtQuantidade.Text != string.Empty)
            {
                txtValorTotal.Text = (decimal.Parse(txtValorProduto.Text) * int.Parse(txtQuantidade.Text)).ToString();
            }
        }

        private void btnAdicionarProduto_Click(object sender, EventArgs e)
        {
            string m;
            if (txtNomeProduto.Text != "" && txtQuantidade.Text != "" && txtValorTotal.Text != "" && txtValorProduto.Text != "" && txtCodProduto.Text != "")
            {

                m = new DB().ConsultaEstoque(Convert.ToInt32(txtCodProduto.Text), txtQuantidade.Text);
                if (m.Equals("Compra finalizada!"))
                {
                    dgvCaixa1.Rows.Add(0, 0, txtCodProduto.Text, txtNomeProduto.Text, txtValorTotal.Text.Replace(",", "."), txtQuantidade.Text, txtValorTotal.Text.Replace(",", "."));
                    totalvenda += float.Parse(txtValorTotal.Text);
                    txtValorTotalCompra.Text = totalvenda.ToString();
                    txtCodProduto.Text = "";
                    txtNomeProduto.Text = "";
                    txtValorTotal.Text = "";
                    txtQuantidade.Text = "";
                    txtValorProduto.Text = "";
                    mtbCPF.Focus();
                }
                else if (m.Equals("Quantidade baixa"))
                {
                    MessageBox.Show("O produto não tem a quantidade em estoque para prosseguir!!");
                    txtCodProduto.Text = "";
                    txtNomeProduto.Text = "";
                    txtValorTotal.Text = "";
                    txtQuantidade.Text = "";
                    txtValorProduto.Text = "";
                    mtbCPF.Focus();
                }
            }
            else
            {
                MessageBox.Show("Por favor preencher todos os dados!!");
            }
        }

        private void btnAdicionarServico_Click(object sender, EventArgs e)
        {
            if (txtNomeProduto.Text != "" && txtQuantidade.Text != "" && txtValorTotal.Text != "" && txtValorProduto.Text != "" && txtCodProduto.Text != "")
            {
                dgvCaixa1.Rows.Add(txtCodProduto.Text, txtNomeProduto.Text, 0, 0, txtValorTotal.Text.Replace(",", "."), txtQuantidade.Text, txtValorTotal.Text.Replace(",", "."));
                totalvenda += float.Parse(txtValorTotal.Text);
                txtValorTotalCompra.Text = totalvenda.ToString();
                txtCodProduto.Text = "";
                txtNomeProduto.Text = "";
                txtValorTotal.Text = "";
                txtQuantidade.Text = "";
                txtValorProduto.Text = "";
                cbxServicos.SelectedIndex = -1;
            }
            else
            {
                MessageBox.Show("ERROR");
            }
        }

        private void txtCodProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;

            }
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;

            }
        }

        private void mtbCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }

        private void GerarCodigoVenda()
        {
            materialLabel1.Text = new Conexao.DB().GerarCodigoCompra();
        }

        private void InserirCompra()
        {         
                for (int i = 0; i < dgvCaixa1.Rows.Count - 1; i++)
                {
                    if (dgvCaixa1.Rows[i].Cells[1].Value.ToString().Equals("0"))
                    {
                        new DB().BaixaEstoque(Convert.ToInt32(dgvCaixa1.Rows[i].Cells[2].Value.ToString()), dgvCaixa1.Rows[i].Cells[5].Value.ToString());
                    }
                    new CaixaController().CrudCaixa(Convert.ToInt32(materialLabel1.Text), Convert.ToInt32(dgvCaixa1.Rows[i].Cells[2].Value.ToString()), Convert.ToInt32(dgvCaixa1.Rows[i].Cells[0].Value.ToString()),
                    DateTime.Now.ToString(), decimal.Parse(dgvCaixa1.Rows[i].Cells[6].Value.ToString().Replace(".", ",")), mtbCPF.Text);
                }
                if (rbDinheiro.Checked == true)
                {
                    new CaixaController().CrudPagamento(0, Convert.ToInt32(materialLabel1.Text), decimal.Parse(txtValorTotalCompra.Text), 1);
                }
                if (rbDebito.Checked == true)
                {
                    new CaixaController().CrudPagamento(0, Convert.ToInt32(materialLabel1.Text), decimal.Parse(txtValorTotalCompra.Text), 2);
                }
                MessageBox.Show("Compra feita com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dgvCaixa1.Rows.Clear();
                totalvenda = 0;
                txtValorTotalCompra.Text = "0";
                cbxServicos.SelectedIndex = -1;
                mtbCPF.Text = "";
                GerarCodigoVenda();        
        }
        private void btnFinalizarCompra_Click(object sender, EventArgs e)
        {   if (txtValorTotalCompra.Text != "" && mtbCPF.Text != "")
            {
                InserirCompra();
            }
            else
            {
                MessageBox.Show("Por favor inserir algum serviço ou produto,\n ou um CPF para efetuar a VENDA!!", "Inserir Dados", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void rbCredito_CheckedChanged_1(object sender, EventArgs e)
        {
            if (rbCredito.Checked == true)
            {
                CalculoParcelamento calculoCredito = new CalculoParcelamento(txtValorTotalCompra.Text, materialLabel1.Text);
                calculoCredito.ShowDialog();
            }

        }
    }
}

