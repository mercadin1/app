﻿namespace MercadIn.View
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.caixaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeServiçosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeEmpresaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeProdutosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeCategoriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeEndereçoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produtosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servicosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empresasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.endereçosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatóriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produtosDisponíveisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasEQuitaçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produtosAVencerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitaçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitarFaturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.caixaToolStripMenuItem,
            this.cadastroToolStripMenuItem,
            this.consultaToolStripMenuItem,
            this.relatóriosToolStripMenuItem,
            this.quitarFaturasToolStripMenuItem,
            this.sairToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 27);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(473, 26);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // caixaToolStripMenuItem
            // 
            this.caixaToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.caixaToolStripMenuItem.Name = "caixaToolStripMenuItem";
            this.caixaToolStripMenuItem.Size = new System.Drawing.Size(57, 22);
            this.caixaToolStripMenuItem.Text = "Caixa";
            this.caixaToolStripMenuItem.Click += new System.EventHandler(this.caixaToolStripMenuItem_Click);
            // 
            // cadastroToolStripMenuItem
            // 
            this.cadastroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroDeClienteToolStripMenuItem,
            this.cadastroDeServiçosToolStripMenuItem,
            this.cadastroDeEmpresaToolStripMenuItem,
            this.cadastroDeProdutosToolStripMenuItem,
            this.cadastroDeCategoriaToolStripMenuItem,
            this.cadastroDeEndereçoToolStripMenuItem});
            this.cadastroToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.cadastroToolStripMenuItem.Name = "cadastroToolStripMenuItem";
            this.cadastroToolStripMenuItem.Size = new System.Drawing.Size(81, 22);
            this.cadastroToolStripMenuItem.Text = "Cadastro";
            // 
            // cadastroDeClienteToolStripMenuItem
            // 
            this.cadastroDeClienteToolStripMenuItem.Name = "cadastroDeClienteToolStripMenuItem";
            this.cadastroDeClienteToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.cadastroDeClienteToolStripMenuItem.Text = "Cadastro de Cliente";
            this.cadastroDeClienteToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeClienteToolStripMenuItem_Click);
            // 
            // cadastroDeServiçosToolStripMenuItem
            // 
            this.cadastroDeServiçosToolStripMenuItem.Name = "cadastroDeServiçosToolStripMenuItem";
            this.cadastroDeServiçosToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.cadastroDeServiçosToolStripMenuItem.Text = "Cadastro de Serviços";
            this.cadastroDeServiçosToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeServiçosToolStripMenuItem_Click);
            // 
            // cadastroDeEmpresaToolStripMenuItem
            // 
            this.cadastroDeEmpresaToolStripMenuItem.Name = "cadastroDeEmpresaToolStripMenuItem";
            this.cadastroDeEmpresaToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.cadastroDeEmpresaToolStripMenuItem.Text = "Cadastro de Empresa";
            this.cadastroDeEmpresaToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeEmpresaToolStripMenuItem_Click);
            // 
            // cadastroDeProdutosToolStripMenuItem
            // 
            this.cadastroDeProdutosToolStripMenuItem.Name = "cadastroDeProdutosToolStripMenuItem";
            this.cadastroDeProdutosToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.cadastroDeProdutosToolStripMenuItem.Text = "Cadastro de Produtos";
            this.cadastroDeProdutosToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeProdutosToolStripMenuItem_Click);
            // 
            // cadastroDeCategoriaToolStripMenuItem
            // 
            this.cadastroDeCategoriaToolStripMenuItem.Name = "cadastroDeCategoriaToolStripMenuItem";
            this.cadastroDeCategoriaToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.cadastroDeCategoriaToolStripMenuItem.Text = "Cadastro de Categoria";
            this.cadastroDeCategoriaToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeCategoriaToolStripMenuItem_Click);
            // 
            // cadastroDeEndereçoToolStripMenuItem
            // 
            this.cadastroDeEndereçoToolStripMenuItem.Name = "cadastroDeEndereçoToolStripMenuItem";
            this.cadastroDeEndereçoToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.cadastroDeEndereçoToolStripMenuItem.Text = "Cadastro de Endereço";
            this.cadastroDeEndereçoToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeEndereçoToolStripMenuItem_Click);
            // 
            // consultaToolStripMenuItem
            // 
            this.consultaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clientesToolStripMenuItem,
            this.produtosToolStripMenuItem,
            this.servicosToolStripMenuItem,
            this.empresasToolStripMenuItem,
            this.categoriasToolStripMenuItem,
            this.endereçosToolStripMenuItem});
            this.consultaToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.consultaToolStripMenuItem.Name = "consultaToolStripMenuItem";
            this.consultaToolStripMenuItem.Size = new System.Drawing.Size(79, 22);
            this.consultaToolStripMenuItem.Text = "Consulta";
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.clientesToolStripMenuItem.Text = "Clientes";
            this.clientesToolStripMenuItem.Click += new System.EventHandler(this.clientesToolStripMenuItem_Click);
            // 
            // produtosToolStripMenuItem
            // 
            this.produtosToolStripMenuItem.Name = "produtosToolStripMenuItem";
            this.produtosToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.produtosToolStripMenuItem.Text = "Produtos";
            this.produtosToolStripMenuItem.Click += new System.EventHandler(this.produtosToolStripMenuItem_Click);
            // 
            // servicosToolStripMenuItem
            // 
            this.servicosToolStripMenuItem.Name = "servicosToolStripMenuItem";
            this.servicosToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.servicosToolStripMenuItem.Text = "Servicos";
            this.servicosToolStripMenuItem.Click += new System.EventHandler(this.servicosToolStripMenuItem_Click);
            // 
            // empresasToolStripMenuItem
            // 
            this.empresasToolStripMenuItem.Name = "empresasToolStripMenuItem";
            this.empresasToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.empresasToolStripMenuItem.Text = "Empresas";
            this.empresasToolStripMenuItem.Click += new System.EventHandler(this.empresasToolStripMenuItem_Click);
            // 
            // categoriasToolStripMenuItem
            // 
            this.categoriasToolStripMenuItem.Name = "categoriasToolStripMenuItem";
            this.categoriasToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.categoriasToolStripMenuItem.Text = "Categorias";
            this.categoriasToolStripMenuItem.Click += new System.EventHandler(this.categoriasToolStripMenuItem_Click);
            // 
            // endereçosToolStripMenuItem
            // 
            this.endereçosToolStripMenuItem.Name = "endereçosToolStripMenuItem";
            this.endereçosToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.endereçosToolStripMenuItem.Text = "Endereços";
            this.endereçosToolStripMenuItem.Click += new System.EventHandler(this.endereçosToolStripMenuItem_Click);
            // 
            // relatóriosToolStripMenuItem
            // 
            this.relatóriosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produtosDisponíveisToolStripMenuItem,
            this.comprasEQuitaçõesToolStripMenuItem,
            this.produtosAVencerToolStripMenuItem,
            this.quitaçõesToolStripMenuItem});
            this.relatóriosToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.relatóriosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop;
            this.relatóriosToolStripMenuItem.Name = "relatóriosToolStripMenuItem";
            this.relatóriosToolStripMenuItem.Size = new System.Drawing.Size(88, 22);
            this.relatóriosToolStripMenuItem.Text = "Relatórios";
            // 
            // produtosDisponíveisToolStripMenuItem
            // 
            this.produtosDisponíveisToolStripMenuItem.Name = "produtosDisponíveisToolStripMenuItem";
            this.produtosDisponíveisToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.produtosDisponíveisToolStripMenuItem.Text = "Produtos Disponíveis";
            this.produtosDisponíveisToolStripMenuItem.Click += new System.EventHandler(this.produtosDisponíveisToolStripMenuItem_Click);
            // 
            // comprasEQuitaçõesToolStripMenuItem
            // 
            this.comprasEQuitaçõesToolStripMenuItem.Name = "comprasEQuitaçõesToolStripMenuItem";
            this.comprasEQuitaçõesToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.comprasEQuitaçõesToolStripMenuItem.Text = "Compras e Quitações ";
            this.comprasEQuitaçõesToolStripMenuItem.Click += new System.EventHandler(this.comprasEQuitaçõesToolStripMenuItem_Click);
            // 
            // produtosAVencerToolStripMenuItem
            // 
            this.produtosAVencerToolStripMenuItem.Name = "produtosAVencerToolStripMenuItem";
            this.produtosAVencerToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.produtosAVencerToolStripMenuItem.Text = "Produtos a Vencer ";
            this.produtosAVencerToolStripMenuItem.Click += new System.EventHandler(this.produtosAVencerToolStripMenuItem_Click);
            // 
            // quitaçõesToolStripMenuItem
            // 
            this.quitaçõesToolStripMenuItem.Name = "quitaçõesToolStripMenuItem";
            this.quitaçõesToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.quitaçõesToolStripMenuItem.Text = "Faturas em Atraso";
            this.quitaçõesToolStripMenuItem.Click += new System.EventHandler(this.quitaçõesToolStripMenuItem_Click);
            // 
            // quitarFaturasToolStripMenuItem
            // 
            this.quitarFaturasToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.quitarFaturasToolStripMenuItem.Name = "quitarFaturasToolStripMenuItem";
            this.quitarFaturasToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.quitarFaturasToolStripMenuItem.Text = "Quitar Faturas";
            this.quitarFaturasToolStripMenuItem.Click += new System.EventHandler(this.quitarFaturasToolStripMenuItem_Click);
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.sairToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(46, 22);
            this.sairToolStripMenuItem.Text = "Sair";
            this.sairToolStripMenuItem.Click += new System.EventHandler(this.sairToolStripMenuItem_Click);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.Indigo;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(-1, -1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(797, 30);
            this.panel4.TabIndex = 62;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(13, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Menu";
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MercadIn.Properties.Resources.Logo_MercadIn1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(795, 609);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Menu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem caixaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem servicosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeServiçosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeEmpresaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empresasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeProdutosToolStripMenuItem;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeCategoriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoriasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeEndereçoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem endereçosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatóriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtosDisponíveisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtosAVencerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comprasEQuitaçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitaçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitarFaturasToolStripMenuItem;
    }
}