﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MercadIn.Model;
using MercadIn.View;
using System.Windows.Forms;

namespace MercadIn.Controller
{
    class FaturaController
    {
        public void ListarFaturas(DataGridView obj, int id)
        {
            obj.DataSource = new Fatura().ListarFaturas(id);
        }

        public bool Pagar(int id)
        {
            return new Fatura().Pagamento(id);
        }
        public void faturas(DataGridView obj,int id)
        {
           obj.DataSource = new Fatura().Relatorio(id);
        }
    }
}
