﻿using MercadIn.Conexao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MercadIn.Model
{
    class Funcionario
    {
        public int id { get; set; }
        public string usuario { get; set; }
        public string senha { get; set; }
        public bool ativo { get; set; }

        public int BuscarFuncionario(string usuario, string senha)
        {
            int resultado = 0;
            DataTable dt = new DB().Select("SELECT nivelAcesso FROM vini.Funcionario WHERE ativo = TRUE AND usuario = '" + usuario + "' AND senha = '" + senha + "'");
            try
            {
                foreach (DataRow linha in dt.Rows)
                    foreach (object obj in linha.ItemArray)
                        resultado = Convert.ToInt32(obj.ToString());
            }
            catch(Exception ex)
            {
                resultado = 0;
            }
            return resultado;
        }
    }
}
