﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MercadIn.Conexao;
using System.Data;

namespace MercadIn.Model
{
    class Pagamento
    {
        public int id { get; set; }
        public int idCompra { get; set; }
        public decimal valor { get; set; }
        public int idTipoPagamento { get; set; }
        public bool CrudPagamento(Pagamento obj)
        {
            string strSQL;
            if (obj.id == 0)
            {
                strSQL = "insert into vini.Pagamento (idCompra, valor, idTipoPagamento) values(";                
                strSQL += +obj.idCompra + ",";
                strSQL += "'" +obj.valor + "',";
                strSQL += +obj.idTipoPagamento + ")";
                return new DB().Insert(strSQL);
            }
            else
            {
                return false;
            }
        }

    }
}
