﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MercadIn.Conexao;

namespace MercadIn.Model
{
    class Fatura
    {
        public DataTable ListarFaturas(int id)
        {
            return new DB().Select("SELECT DISTINCT F.* FROM vini.Fatura F INNER JOIN vini.Pagamento P ON P.id = F.idPagamento INNER JOIN vini.Compra C ON C.id = P.idCompra where C.idCliente = " + id);
        }

        public bool Pagamento(int id)
        {
            return new DB().Update("update vini.Fatura set dataPagamento = date_add(current_date(), interval + 0 day) where id =" + id);
        }

        public DataTable Relatorio(int id)
        {
            return new DB().Select("SELECT DISTINCT F.*, CASE WHEN F.dataPagamento IS NULL THEN 'Não' ELSE 'Sim' END Pago FROM vini.Fatura F INNER JOIN vini.Pagamento P ON P.id = F.idPagamento INNER JOIN vini.Compra C ON C.id = P.idCompra where C.idCliente = " +id);
        }
    }
}
