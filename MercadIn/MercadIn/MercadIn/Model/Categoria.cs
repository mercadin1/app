﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MercadIn.Conexao;

namespace MercadIn.Model
{
    class Categoria
    {
        public int id { get; set; }
        public string nome { get; set; }
        
        public bool Crud(Categoria cate)
        {
            string strSQL;
            if(cate.id == 0)
            {
                strSQL = "insert into vini.Categoria (nome) values (";
                strSQL += "'" + cate.nome + "')";
                return new DB().Insert(strSQL);
            }
            else
            {
                strSQL = "update vini.Categoria set nome = '" + cate.nome + "' where id = " + cate.id;
                return new DB().Update(strSQL);
            }
        }

        public DataTable ListarCategoria()
        {
            return new DB().Select("select * from vini.Categoria");
        }

        public bool deletar (int id)
        {
            return new DB().Delete("delete from vini.Categoria WHERE id= " + id);
        }
    }            
}
