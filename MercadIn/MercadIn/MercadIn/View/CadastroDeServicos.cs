﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class CadastroDeServicos : MaterialSkin.Controls.MaterialForm // Atribuindo Design do MaterialSkin no form.
    {
        public CadastroDeServicos()
        {
            InitializeComponent();
            // Definindo design com bibliotécas do Material Skin
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
        }

        private void CadastroDeServicos_Load(object sender, EventArgs e)
        {

        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            // Verificação se todos os campos estiverem com dados setados para poder continuar se os campos não estiverem completos ira
            // Exibir uma mensagem "Preencha os campos obrigatórios!".
            if (txtNome.Text != "" && txtDescricao.Text != "" && txtPreco.Text != "")
            {
                // Enviando todos os dados necessários para o ServicoController para executar o método EnderecoCrud se estiver tudo ok ele exibe um
                // MessageBox "Serviço adicionado com sucesso", depois limpa os campos, se não exibe uma messagem " Erro ao adicionar o Serviço ".
                if (new ServicoController().CrudServicos(txtNome.Text, 0, txtDescricao.Text, decimal.Parse(txtPreco.Text.Replace(".", ","))))
                {
                    MessageBox.Show("Serviço adicionado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LimparCampos();
                }
                else
                {
                    MessageBox.Show("Erro ao adicionar o Serviço", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Preencha os campos obrigatórios!", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close(); // Fechando a view.
        
        }

        public void LimparCampos() // Método para limpar os campos.
        {
            txtDescricao.Text = "";
            txtNome.Text = "";
            txtPreco.Text = "";
        }
        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimparCampos(); // Puxando método de limpar os campos.
        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 46) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }
    }
    }

