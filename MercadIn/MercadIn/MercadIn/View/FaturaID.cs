﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class FaturaID : MaterialSkin.Controls.MaterialForm
    {
        
        public FaturaID()
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
        }

        private void FaturaID_Load(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtID.Text != "")
            {
                this.Hide();
                FaturasAtraso fat = new FaturasAtraso(txtID.Text);
                fat.ShowDialog();
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtID.Text = "";
        }
    }
}
