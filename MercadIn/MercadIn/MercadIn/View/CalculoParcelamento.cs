﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class CalculoParcelamento : MaterialSkin.Controls.MaterialForm
    {
        string VALORTOTAL;
        string IDCOMPRA;
        decimal result;
        public CalculoParcelamento(string valortotal, string idCompra)
        {           
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            VALORTOTAL = valortotal;
            IDCOMPRA = idCompra;
        }

        private void CalculoCredito_Load(object sender, EventArgs e)
        {
            txtValor.Text = VALORTOTAL;
            txtID.Text = IDCOMPRA;
        }

        private void txtDesejado_KeyPress(object sender, KeyPressEventArgs e)
        {
            //result = float.Parse(txtValor.Text) / float.Parse(txtDesejado.Text);
            //txtDemonstrativo.Text = result.ToString();
            //txtValor.Focus();
        }      
        private void txtDesejado_Validating(object sender, CancelEventArgs e)
        {
            result = Math.Round( decimal.Parse(txtValor.Text) / decimal.Parse(txtDesejado.Text),2);
            txtDemonstrativo.Text = result.ToString();
            txtValor.Focus();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool resultado = false;
            int fatura = 30;
            for (int i = 0; i < Convert.ToInt32(txtDesejado.Text); i++)
            {
                resultado = new CaixaController().CrudPagamento(0, Convert.ToInt32(txtID.Text), decimal.Parse(txtDemonstrativo.Text.Replace(".", ",")), 3);
                new CaixaController().Fatura(decimal.Parse(txtDemonstrativo.Text.Replace(".", ",")), fatura);
                fatura += 30;
            }
            if (resultado)
            {
                MessageBox.Show("Produto parcelado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtDesejado.Text = "";
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
