﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MercadIn.Model;
using MercadIn.View;


namespace MercadIn.Controller
{
    class ClienteController
    {
         
        public bool CrudCliente(int id, string cpf, string contato, string nome, string email, string dataNascimento)
        {
            Cliente cli = new Cliente();
            cli.id = id;
            cli.cpf = cpf;
            cli.contato = contato;
            cli.nome = nome;
            cli.email = email;
            cli.dataNascimento = dataNascimento;
            return new Cliente().Crud(cli);        
        }

        public void Listar(DataGridView objetoResposta)
        {
            // Verifica se há opções de seleção diferentes feitas na View
            // (possivelmente por meio de sobrecarga de métodos)

            objetoResposta.DataSource = new Cliente().ListarClientes();
        }
        public void ListarComprasQuitacoes(DataGridView objetoResposta,int id)
        {
            objetoResposta.DataSource = new Cliente().ListarComprasQuitacoes(id); 
        }
        public bool Deletar(int id)
        {
            return new Cliente().Deletar(id);
        }   
    }
}
