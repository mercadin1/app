﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MercadIn.Model;
using MercadIn.Controller;
using MercadIn.Conexao;

namespace MercadIn.View
{
    public partial class MenuCliente : Form
    {
        int ID;
        public MenuCliente(int id)
        {
            InitializeComponent();           
            ID = id;
            string nomecliente = new DB().BuscarNomeCliente(id);
            label1.Text = "Olá, " + nomecliente;
        }

        private void MenuCliente_Load(object sender, EventArgs e)
        {
            dgvGeral.Visible = false;
            btnFechar.Visible = false;
        }

        private void materialFlatButton4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login lg = new Login();
            lg.ShowDialog();
        }

        private void materialFlatButton2_Click(object sender, EventArgs e)
        {
            new ClienteController().ListarComprasQuitacoes(dgvGeral, ID);
            dgvGeral.Visible = true;
            btnFechar.Visible = true;
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            dgvGeral.Visible = false;
            btnFechar.Visible = false;
        }

        private void materialFlatButton3_Click(object sender, EventArgs e)
        {
            new FaturaController().faturas(dgvGeral, ID);
            dgvGeral.Visible = true;
            btnFechar.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new ClienteController().ListarComprasQuitacoes(dgvGeral, ID);
            dgvGeral.Visible = true;
            btnFechar.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new FaturaController().faturas(dgvGeral, ID);
            dgvGeral.Visible = true;
            btnFechar.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login lg = new Login();
            lg.ShowDialog();
        }
    }
}
