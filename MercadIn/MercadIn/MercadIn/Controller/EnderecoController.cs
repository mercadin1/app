﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MercadIn.Model;
using MercadIn.View;
using System.Windows.Forms;

namespace MercadIn.Controller
{
    class EnderecoController
    {

        public bool EnderecoCrud(int id, int idCliente, string cep, string cidade, string estado, string rua, string complemento)
        {
            Endereco end = new Endereco();
            end.id = id;
            end.idCliente = idCliente;
            end.cep = cep;
            end.cidade = cidade;
            end.estado = estado;
            end.rua = rua;
            end.complemento = complemento;

            return new Endereco().CrudEndereco(end);
        }
        public void ListarEndereco(DataGridView obj)
        {
            obj.DataSource = new Endereco().ListaEnderecos();
        }
        public bool Deletar(int id)
        {
            return new Endereco().Deletar(id);
        }
    }
}
