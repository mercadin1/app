﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MercadIn.Model;
using MercadIn.View;
using System.Windows.Forms;

namespace MercadIn.Controller
{
    class ProdutoController
    {
        public bool ProdutoCrud(int id,string dataProduto, int quantidade, decimal precoVenda, decimal precoCompra, string nome, string codigoBarras, int idCategoria, int idEmpresa)
        {
            Produto produto = new Produto();
            produto.id = id;
            produto.dataProduto = dataProduto;
            produto.quantidade = quantidade;
            produto.precoVenda = precoVenda;
            produto.precoCompra = precoCompra;
            produto.nome = nome;
            produto.codigoBarras = codigoBarras;
            produto.idCategoria = idCategoria;
            produto.idEmpresa = idEmpresa;

            return new Produto().CrudProduto(produto);
        }
        
        public void ListarProduto(DataGridView objetoResposta)
        {
            objetoResposta.DataSource = new Produto().ListarProdutos();
        }
        public void ListarEstoque(DataGridView obj)
        {
            obj.DataSource = new Produto().ListarEstoque();
        }
        public void ListarProdutos(DataGridView objetoResposta)
        {
            objetoResposta.DataSource = new Produto().ListarProduto();
        }

        public Produto SelecionarProduto(string id)
        {
            try
            {
                return new Produto().ConsultarProduto(Convert.ToInt32(id.Trim()));
            }
            catch
            {
                return null;
            }
        }
        public void ListarProdutosVencer(DataGridView dgv)
        {
            
            dgv.DataSource = new Produto().ListarProdutosVencer();
        }
        public bool Deletar(int id)
        {
            return new Produto().Deletar(id);
        }
    }
}
