﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;

namespace MercadIn.View
{
    public partial class Login : MaterialSkin.Controls.MaterialForm
    {
        public Login()
        {
            InitializeComponent();
            txtSenha.PasswordChar = '*';
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
        }
        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)

            {
                btnLogar_Click(sender, e);
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            Splash splash = new Splash();
            splash.ShowDialog();
        }

        private void materialFlatButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogar_Click(object sender, EventArgs e)
        {
            if(txtUsuario.Text != "" && txtSenha.Text != "")
            {
                int resultado = new Model.Funcionario().BuscarFuncionario(txtUsuario.Text, txtSenha.Text);
                if (resultado > 0)
                {
                    this.Hide();
                    if(resultado == 1)
                    {
                        Menu menu = new Menu();
                        menu.ShowDialog();
                    }
                    if (resultado == 2)
                    {
                        MenuFuncionario menuFuncionario = new MenuFuncionario();
                        menuFuncionario.ShowDialog();
                    }
                } else
                {
                    resultado = new Model.Cliente().BuscarCliente(txtUsuario.Text, txtSenha.Text);
                    if (resultado == 0)
                        MessageBox.Show("Usuário e/ou senha errados \n Tente novamente mais tarde!", "Usuário e/ou senha inválido!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        this.Hide();
                        MenuCliente mc = new MenuCliente(resultado);
                        mc.ShowDialog();
                    }
                }               
            } else
            {
                MessageBox.Show("É necessário digitar usuário e/ou senha para logar", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtSenha_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)

            {
                btnLogar_Click(sender, e);
            }
        }
    }
}
