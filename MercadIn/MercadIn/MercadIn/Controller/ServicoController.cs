﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MercadIn.Model;
using MercadIn.View;
using System.Windows.Forms;

namespace MercadIn.Controller
{
    class ServicoController
    {

        public bool CrudServicos(string nome, int id,string descricao, decimal preco)
        {
            Servicos serv = new Servicos();
            serv.nome = nome;
            serv.id = id;
            serv.descricao = descricao;
            serv.preco = preco;
            
            return new Servicos().Crud(serv);
        }

        public void ListarServicos(DataGridView objetoResposta)
        {
            objetoResposta.DataSource = new Servicos().ListarServicos();
        }

        public Servicos SelecionarServico(string id)
        {
            try
            {
                return new Servicos().ConsultarServicos(Convert.ToInt32(id.Trim()));
            }
            catch
            {
                return null;
            }
        }
        public bool Deletar(int id)
        {
            return new Servicos().Deletar(id);
        }
    }
}
