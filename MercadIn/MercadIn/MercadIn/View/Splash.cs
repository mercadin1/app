﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MercadIn.View
{
    public partial class Splash : Form
    {
        private bool tela = true;
        public Splash()
        {
            InitializeComponent();
        }

        private void Splash_Load(object sender, EventArgs e)
        {
            
        }

        private void tmrSplash_Tick(object sender, EventArgs e)
        {
            Opacity -= 0.03;
            if (Opacity <= 0)
            {
                tmrSplash.Enabled = false;
                this.Close();
            }
        }
    }
}
