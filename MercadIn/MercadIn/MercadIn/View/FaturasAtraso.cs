﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using DGVPrinterHelper;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class FaturasAtraso : MaterialSkin.Controls.MaterialForm
    {
        string ID;
        public FaturasAtraso(string id)
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            ID = id;
        }

        private void Atualizar()
        {
            new FaturaController().faturas(dgvFaturasAtraso, Convert.ToInt32(ID));
        }
        private void FaturasAtraso_Load(object sender, EventArgs e)
        {
            Atualizar();
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            DGVPrinter print = new DGVPrinter();
            print.Title = "RELATÓRIO DO CLIENTE\n\n\n";
            print.SubTitleAlignment = StringAlignment.Near;
            print.SubTitle = string.Format("DATA: {0}", DateTime.Now.Date.ToString("dd/MM/yyyy"));
            print.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
            print.PageNumbers = true;
            print.PageNumberInHeader = false;
            print.PorportionalColumns = true;
            print.HeaderCellAlignment = StringAlignment.Center;
            print.Footer = "Mercadin";
            print.FooterSpacing = 15;
            print.PrintDataGridView(dgvFaturasAtraso);
        }
    }
}
