﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class AlteracaoCategoria : MaterialSkin.Controls.MaterialForm // Atribuindo Design do MaterialSkin no form.
    {
        // Vars para receber ID,NOME do ConsultarCategoria
        public string ID; 
        public string NOME;
        public AlteracaoCategoria(string id, string nome)
        {
            InitializeComponent();
            // Definindo design com bibliotécas do Material Skin
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            // Setando os valores nas variaveis 
            ID = id;
            NOME = nome;
        }

        private void AlteracaoRemocaoCategoria_Load(object sender, EventArgs e)
        {
            // Atribuindo os valores que estão nas vars, para os campos de textos.
            txtID.Text = ID;
            txtNomeEmpresa.Text = NOME;
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            // Evento de fechar tela.
            this.Hide();
        }

        public void LimparCampos() // Método para limpar os campos
        {
            txtID.Text = "";
            txtNomeEmpresa.Text = "";
        }
        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimparCampos(); // Puxando método para limpar os campos
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            // Mandando os valores para CategoriaController, no método CrudCategoria caso de certo na alteração ira aparecer um
            // MessageBox, depois limpar os campos , fechar a tela e abrir a ConsultarCategoria
            // Se não aparece um MessageBox, com erro.
            if (new CategoriaController().CrudCategoria(int.Parse(txtID.Text), txtNomeEmpresa.Text))
            {
                MessageBox.Show("Categoria alterado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LimparCampos();
                Hide();
                ConsultarCategoria cc = new ConsultarCategoria(1);
                cc.ShowDialog();
            }
            else
            {
                MessageBox.Show("Erro ao alterar a Categoria!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
