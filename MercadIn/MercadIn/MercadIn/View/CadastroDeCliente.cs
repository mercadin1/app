﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;


namespace MercadIn.View
{
    public partial class CadastroDeCliente : MaterialSkin.Controls.MaterialForm //instancia do design do form do materialskin.
    {        
        public CadastroDeCliente()
        {
            InitializeComponent();
            // MaterialSkin bibliotecas para setar o design no form.
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
        }
        public void LimparCampos() // Método Limpar os Campos
        {
            txtNome.Text = "";
            txtEmail.Text = "";
            mtbDataNascimento.Text = "";
            mtbCPF.Text = "";
            maskedTextBox1.Text = "";
        }
        private void CadastroDeCliente_Load(object sender, EventArgs e)
        {

        }

        private void btnCadastrarCliente_Click(object sender, EventArgs e)
        {
            // Verificação se todos os campos estiverem com dados setados para poder continuar se os campos não estiverem completos ira
            // Exibir uma mensagem "Preencha os campos obrigatórios!".
            if (mtbCPF.Text != "" && maskedTextBox1.Text != "" && mtbDataNascimento.Text != "" && txtNome.Text != "" && txtEmail.Text !=  "") 
            {
                // Enviando todos os dados necessários para o ClienteController para executar o método CrudCliente se estiver tudo ok ele exibe um
                // MessageBox "Cliente adicionado com sucesso", e limpando os campos, se não exibe uma messagem "Erro ao adicionar o cliente ".               
                if (new ClienteController().CrudCliente(0, mtbCPF.Text, maskedTextBox1.Text, txtNome.Text, txtEmail.Text, mtbDataNascimento.Text)) { 
                     MessageBox.Show("Cliente adicionado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LimparCampos();
                }
                else
                {
                    MessageBox.Show("Erro ao adicionar o cliente", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Preencha os campos obrigatórios!", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void materialFlatButton3_Click(object sender, EventArgs e)
        {
            this.Close(); // Fechando a view.
        }

        private void materialFlatButton2_Click(object sender, EventArgs e)
        {
            LimparCampos();// Puxando o método de limpar os campos.
        }

        private void mtbDataNascimento_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }

        private void txtContato_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }

        private void mtbCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }
    }    
 }

