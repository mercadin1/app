﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class AlteracaoProdutos : MaterialSkin.Controls.MaterialForm // Atribuindo Design do MaterialSkin no form.
    {
        // Vars para receber do ConsultarProdutoss
        string ID;
        string DATA;
        string QUANTIDADE;
        string PRECOCOMPRA;
        string PRECOVENDA;
        string NOME;
        string CODIGOBARRAS;
        public AlteracaoProdutos(string id, string data, string quantidade, string precocompra, string precovenda, string nome, string codigobarras)
        {
           
            InitializeComponent();
            // Definindo design com bibliotécas do Material Skin
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            // Setando os valores nas variaveis 
            ID = id;
            DATA = data;
            QUANTIDADE = quantidade;
            PRECOCOMPRA = precocompra;
            PRECOVENDA = precovenda;
            NOME = nome;
            CODIGOBARRAS = codigobarras;
        }

        private void AlteracaoRemocaoProdutos_Load(object sender, EventArgs e)
        {
            //Puxando os nome das empresas e setando os id nos valores no cbxEmpresa "ComboBox" para poder tratar-los na view.
            cbxEmpresa.DataSource = new Empresa().ListarEmpresas();
            cbxEmpresa.DisplayMember = "Nome";
            cbxEmpresa.ValueMember = "id";
            cbxEmpresa.SelectedIndex = -1;
            //Puxando os nome das categorias e setando os id nos valores no cbxCategoria "ComboBox" para poder tratar-los na view.
            cbxCategoria.DataSource = new Categoria().ListarCategoria();
            cbxCategoria.DisplayMember = "Nome";
            cbxCategoria.ValueMember = "id";
            cbxCategoria.SelectedIndex = -1;
            // Atribuindo os valores que estão nas vars, para os campos de textos.
            txtNome.Text = NOME;
            txtCompra.Text = PRECOCOMPRA;
            txtVenda.Text = PRECOVENDA;
            txtQuantidadeEstoque.Text = QUANTIDADE;
            txtCodigoBarras.Text = CODIGOBARRAS;
            maskedTextBox1.Text = DATA;
            txtID.Text = ID;
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close(); // Evento de fechar tela.
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            // Mandando os valores para ProdutoController, no método ProdutoCrud caso de certo na ateração ira aparecer um
            // Messagebox, depois fechando a tela e abrindo a tela ConsultarProdutoss
            // Se não aparece um MessageBox, com erro.
            if (new ProdutoController().ProdutoCrud(int.Parse(txtID.Text), maskedTextBox1.Text, Convert.ToInt32(txtQuantidadeEstoque.Text),
                decimal.Parse(txtVenda.Text), decimal.Parse(txtCompra.Text), txtNome.Text, txtCodigoBarras.Text, int.Parse(cbxCategoria.SelectedValue.ToString()), int.Parse(cbxEmpresa.SelectedValue.ToString())))
            {
                MessageBox.Show("Produto alterado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Hide();
                ConsultarProdutoss consultarProdutoss = new ConsultarProdutoss(1);
                consultarProdutoss.ShowDialog();
            }
            else
            {
                MessageBox.Show("Erro ao alterar o cliente!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
