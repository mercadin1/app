﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;


namespace MercadIn.Conexao
{
    class DB
    {
        private MySqlConnection connection;
       
        /// <summary>
        /// Cria uma conexão com um banco de dados MySQL utilizando as informações padrões.
        /// </summary>
        public DB()
        {

            string connectionString = new Conexao().getConnectionString();
                

            connection = new MySqlConnection(connectionString);
        }

        private bool OpenConnection()
        {
            // Inicializa uma conexão para realizar uma query.
            try
            {
                connection.Open();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao abrir conexão: " + ex.Message, "Erro");
                return false;
            }
        }
        private bool CloseConnection()
        {
            // Finaliza uma conexão após realizar uma query.
            try
            {
                connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao fechar conexão: " + ex.Message, "Erro");
                return false;
            }
        }

        /// <summary>
        /// Método para executar uma query de Select na conexão realizada anteriormente ao criar o objeto DB.
        /// 
        /// Pode lançar uma exceção interna e apresentar o erro por meio de um MessageBox.
        /// </summary>
        /// <param name="query">Query no formato SQL para ser executada no SGBD.</param>
        /// <returns>Retorna um DataTable caso exista a informação no Banco de Dados</returns>
        public DataTable Select(string query)
        {
            DataTable tbRetorno = null;

            try
            {
                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    // Objeto de resposta padrão de uma execução de query
                    MySqlDataReader data = cmd.ExecuteReader();
                    List<string> registros = new List<string>();

                    // Convertendo o DataReader para um DataTable (formato de visualização melhor)
                    DataTable tbEsquema = data.GetSchemaTable();
                    tbRetorno = new DataTable();
                    if (data != null && tbEsquema != null)
                    {
                        // Criando as colunas
                        foreach (DataRow linha in tbEsquema.Rows)
                        {
                            if (!tbRetorno.Columns.Contains(linha["ColumnName"].ToString()))
                            {
                                DataColumn col = new DataColumn()
                                {
                                    ColumnName = linha["ColumnName"].ToString(),
                                    Unique = Convert.ToBoolean(linha["IsUnique"]),
                                    AllowDBNull = Convert.ToBoolean(linha["AllowDBNull"]),
                                    ReadOnly = Convert.ToBoolean(linha["IsReadOnly"])
                                };
                                tbRetorno.Columns.Add(col);
                            }
                        }

                        // Lendo linha a linha os registros da tabela de retorno
                        while (data.Read())
                        {
                            //string registroAtual = "";
                            //for (int i = 0; i < data.FieldCount; i++)
                            //    registroAtual += data.GetValue(i) + " // ";
                            //registros.Add(registroAtual);
                            DataRow novaLinha = tbRetorno.NewRow();
                            // Populando as colunas de resposta com os valores do DB
                            for (int i = 0; i < tbRetorno.Columns.Count; i++)
                            {
                                novaLinha[i] = data.GetValue(i);
                            }
                            tbRetorno.Rows.Add(novaLinha);
                        }

                        data.Close();

                        for (int i = 0; i < registros.Count; i++)
                            Console.WriteLine(registros[i]);
                    }
                    this.CloseConnection();

                    // Tudo ocorreu corretamente e estamos retornando a tabela com as informações.
                    return tbRetorno;
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro MySQL: " + ex.Message, "Erro");
            }

            // A query não possuía uma resposta, então não se retorna nada.
            return null;
        }
        public List<string> SelectData(string query)
        {
            List<string> datas = new List<string>();
            try
            {
                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    // Objeto de resposta padrão de uma execução de query
                    MySqlDataReader data = cmd.ExecuteReader();

                    // Lendo linha a linha os registros da tabela de retorno
                    if (data != null)
                        while (data.Read())
                            datas.Add(data.GetValue(0).ToString());

                    data.Close();
                    this.CloseConnection();

                    // Tudo ocorreu corretamente e estamos retornando a tabela com as informações.
                    return datas;
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro MySQL: " + ex.Message, "Erro");
            }

            // A query não possuía uma resposta, então não se retorna nada.
            return null;
        }

        /// <summary>
        /// Executa uma query de Insert no Banco de Dados MySQL com as configurações já
        /// realizadas.
        /// 
        /// Caso a query esteja em um formato incorreto, uma exceção interna é lançada e mostrada ao usuário por meio de um MessageBox.
        /// </summary>
        /// <param name="query">Query no formato SQL para ser executada no SGBD.</param>
        /// <returns>Retorna se a execução da query teve sucesso ou não.</returns>
        public bool Insert(string query)
        {
            try
            {
                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    int resposta = cmd.ExecuteNonQuery();
                    Console.WriteLine("Resposta insert: " + resposta);
                    this.CloseConnection();
                    if (resposta != 0)
                        return true;
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro MySQL: " + ex.Message, "Erro");
            }
            return false;
        }

        /// <summary>
        /// Executa uma query de Update no Banco de Dados MySQL com as configurações já
        /// realizadas.
        /// 
        /// Caso a query esteja em um formato incorreto, uma exceção interna é lançada e mostrada ao usuário por meio de um MessageBox.
        /// </summary>
        /// <param name="query">Query no formato SQL para ser executada no SGBD.</param>
        /// <returns>Retorna se a execução da query teve sucesso ou não.</returns>
        public bool Update(string query)
        {
            /* A execução das querys de Insert, Update e Delete são iguais.
            Por isso estamos chamando a mesma função já codificada. */
            return Insert(query);
        }

        /// <summary>
        /// Executa uma query de Delete no Banco de Dados MySQL com as configurações já
        /// realizadas.
        /// 
        /// Caso a query esteja em um formato incorreto, uma exceção interna é lançada e mostrada ao usuário por meio de um MessageBox.
        /// </summary>
        /// <param name="query">Query no formato SQL para ser executada no SGBD.</param>
        /// <returns>Retorna se a execução da query teve sucesso ou não.</returns>
        public bool Delete(string query)
        {
            /* A execução das querys de Insert, Update e Delete são iguais.
            Por isso estamos chamando a mesma função já codificada. */
            return Insert(query);
        }

        public Model.Servicos ConsultarServico(int id)
        {
          string  strMySql = "select * from Servicos where id='" + id + "'";
            MySqlCommand comando = new MySqlCommand(strMySql, connection);

            try
            {
                connection.Open();
                MySqlDataReader dr = comando.ExecuteReader();
                dr.Read();
                Model.Servicos se = new Model.Servicos();

                if (!dr.HasRows)
                {
                    return null;   
                }
                else
                {
                    se.id = Convert.ToInt32(dr["id"].ToString());
                    se.nome = dr["nome"].ToString();                    
                    se.preco = decimal.Parse(dr["preco"].ToString());
                    return se;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            finally
            {
                connection.Close();
            }
        }
        public Model.Produto ConsultarProduto(int id)
        {
            string strMySql = "select * from Produtos where id='" + id + "'";
            MySqlCommand comando = new MySqlCommand(strMySql, connection);

            try
            {
                connection.Open();
                MySqlDataReader dr = comando.ExecuteReader();
                dr.Read();
                Model.Produto pr = new Model.Produto();

                if (!dr.HasRows)
                {
                    return null;
                }
                else
                {
                    pr.id = Convert.ToInt32(dr["id"].ToString());
                    pr.nome = dr["nome"].ToString();
                    pr.precoVenda = decimal.Parse(dr["precoVenda"].ToString());                   
                    return pr;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            finally
            {
                connection.Close();
            }
        }
        public int SelecionarCliente(string cpf)
        {
           string strMySql = "select id from vini.Cliente where cpf = '" + cpf + "'";
            try
            {

                connection.Open();
                MySqlCommand comando = new MySqlCommand(strMySql, connection);
                if (comando.ExecuteScalar() == DBNull.Value)
                {
                    return 1;
                }
                else
                {
                    Int32 ra = Convert.ToInt32(comando.ExecuteScalar());                   
                    return ra;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 1;
            }
            finally
            {
                connection.Close();
            }
        }
        public int SelecionarID(int id)
        {
            string strMySql = "select id from vini.Cliente where id = '" + id + "'";
            try
            {

                connection.Open();
                MySqlCommand comando = new MySqlCommand(strMySql, connection);
                if (comando.ExecuteScalar() == DBNull.Value)
                {
                    return 1;
                }
                else
                {
                    Int32 ra = Convert.ToInt32(comando.ExecuteScalar());
                    return ra;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 1;
            }
            finally
            {
                connection.Close();
            }
        }
        public string GerarCodigoCompra()
        {
           string strMySql = "select max(id) from vini.Compra";
            try
            {

                connection.Open();
                MySqlCommand comando = new MySqlCommand(strMySql, connection);
                if (comando.ExecuteScalar() == DBNull.Value)
                {
                    return "1";
                }
                else
                {
                    Int32 ra = Convert.ToInt32(comando.ExecuteScalar()) + 1;
                    return ra.ToString();

                }
            }
            catch (Exception ex)
            {                
                return "1";
            }
            finally
            {
                connection.Close();
            }
        }
        public string BaixaEstoque(int id, string comprado)
        {
            String m;
            try
            {
                OpenConnection();
                MySqlCommand objComando = new MySqlCommand("baixaEstoque", connection);
                objComando.CommandType = CommandType.StoredProcedure;
                objComando.Parameters.AddWithValue("pIdProduto", id);
                objComando.Parameters["pIdProduto"].Direction = ParameterDirection.Input;

                objComando.Parameters.AddWithValue("pComprado", comprado);
                objComando.Parameters["pComprado"].Direction = ParameterDirection.Input;
               
                objComando.Parameters.Add("psaida", MySqlDbType.VarChar, 50);
                objComando.Parameters["psaida"].Direction = ParameterDirection.Output;
                objComando.ExecuteNonQuery();
                m = objComando.Parameters["psaida"].Value.ToString();
                return m;
            }
            catch (Exception erro)
            {
                m = erro.Message;
                return m;
            }
            finally
            {
                CloseConnection();
            }
        }
        public string ConsultaEstoque(int id, string comprado)
        {
            String m;
            try
            {
                OpenConnection();
                MySqlCommand objComando = new MySqlCommand("ConsultaEstoque", connection);
                objComando.CommandType = CommandType.StoredProcedure;
                objComando.Parameters.AddWithValue("pIdProduto", id);
                objComando.Parameters["pIdProduto"].Direction = ParameterDirection.Input;

                objComando.Parameters.AddWithValue("pComprado", comprado);
                objComando.Parameters["pComprado"].Direction = ParameterDirection.Input;

                objComando.Parameters.Add("psaida", MySqlDbType.VarChar, 50);
                objComando.Parameters["psaida"].Direction = ParameterDirection.Output;
                objComando.ExecuteNonQuery();
                m = objComando.Parameters["psaida"].Value.ToString();
                return m;
            }
            catch (Exception erro)
            {
                m = erro.Message;
                return m;
            }
            finally
            {
                CloseConnection();
            }
        }
        public int PuxarIdPagamento()
        {
            string strMySql = "select max(id) from vini.Pagamento";
            try
            {

                connection.Open();
                MySqlCommand comando = new MySqlCommand(strMySql, connection);
                if (comando.ExecuteScalar() == DBNull.Value)
                {
                    return 1;
                }
                else
                {
                    Int32 ra = Convert.ToInt32(comando.ExecuteScalar());
                    return ra;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 1;
            }
            finally
            {
                connection.Close();
            }
        }
        public string BuscarNomeCliente(int id)
        {
            string strMySql = "select nome from vini.Cliente where id = '" + id + "'";
            try
            {

                connection.Open();
                MySqlCommand comando = new MySqlCommand(strMySql, connection);
                if (comando.ExecuteScalar() == DBNull.Value)
                {
                    return "Cliente";
                }
                else
                {
                    string nome = Convert.ToString(comando.ExecuteScalar());
                    return nome;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "Cliente";
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
