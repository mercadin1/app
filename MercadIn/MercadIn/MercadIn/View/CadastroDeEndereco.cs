﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;
using MercadIn.Model;

namespace MercadIn.View
{
    public partial class CadastroDeEndereco : MaterialSkin.Controls.MaterialForm // Atribuindo Design do MaterialSkin no form.
    {
        public CadastroDeEndereco()
        {
            InitializeComponent();
            // Definindo design com bibliotécas do Material Skin
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
        }

        private void CadastroDeEndereco_Load(object sender, EventArgs e)
        {
            // Puxando os nome dos clientes e setando os id nos valores no cbxCliente "ComboBox" para poder tratar-los na view.
            cbxCliente.DataSource = new Cliente().ListarClientes();
            cbxCliente.DisplayMember = "Nome";
            cbxCliente.ValueMember = "id";
            cbxCliente.SelectedIndex = -1;
        }
        public void LimparCampos() // Método para limpar os campos
        {
            txtCep.Text = "";
            txtCidade.Text = "";
            txtComplemento.Text = "";
            txtEstado.Text = "";
            txtRua.Text = "";
            cbxCliente.SelectedIndex = -1;
        }
        private void btnCadastrarCliente_Click(object sender, EventArgs e)
        {
            // Verificação se todos os campos estiverem com dados setados para poder continuar se os campos não estiverem completos ira
            // Exibir uma mensagem "Preencha os campos obrigatórios!".
            if (txtCep.Text != "" && txtCidade.Text != "" && txtComplemento.Text != "" && txtEstado.Text != "" && txtRua.Text != "" && cbxCliente.SelectedIndex != -1)
            {
                // Enviando todos os dados necessários para o EnderecoController para executar o método EnderecoCrud se estiver tudo ok ele exibe um
                // Messagebox "Endereço adicionado com sucesso", depois limpar os campos , se não exibe uma messagem " Erro ao adicionar o Endereço".
                if (new EnderecoController().EnderecoCrud(0, int.Parse(cbxCliente.SelectedValue.ToString()), txtCep.Text, txtCidade.Text, txtEstado.Text, txtRua.Text, txtComplemento.Text)){
                    MessageBox.Show("Endereço adicionado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LimparCampos();
                }
                else
                {
                    MessageBox.Show("Erro ao adicionar o Endereço", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Preencha os campos obrigatórios ou digar corretamente nos campos!", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void materialFlatButton3_Click(object sender, EventArgs e)
        {
            this.Hide(); // Fechando a view.
        }
    }
}
