﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MercadIn.Conexao;

namespace MercadIn.Model
{
   
    class Endereco
    {
        public int id { get; set; }
        public int idCliente { get; set; }
        public string cep { get; set; }
        public string cidade { get; set; }
        public string estado { get; set; }
        public string rua { get; set; }
        public string complemento { get; set; }

        public bool CrudEndereco(Endereco end)
        {

            string strSQL;
            if (end.id == 0)
            {
                strSQL = "insert into vini.Endereco (idCliente, cep, cidade, estado, rua, complemento) values(";
                strSQL += end.idCliente + ",";
                strSQL += "'" + end.cep + "',";
                strSQL += "'" + end.cidade + "',";
                strSQL += "'" + end.estado + "',";
                strSQL += "'" + end.rua + "',";
                strSQL += "'" + end.complemento + "')";
                return new DB().Insert(strSQL);
            }
            else
            {
                strSQL = "update vini.Endereco set cep = '" + end.cep + "',cidade = '" + end.cidade + "',estado = '" + end.estado + "',rua = '" + end.rua + "', complemento = '" + end.complemento + "' where id =" + end.id;
                return new DB().Update(strSQL);
            }

        }
        public DataTable ListaEnderecos()
        {
             return new DB().Select("select * from vini.Endereco");
        }
        public bool Deletar(int id)
        {
            return new DB().Delete("delete from vini.Endereco where id =" + id);
        }

    }
}
