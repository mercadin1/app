﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using DGVPrinterHelper;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class ConsultaEndereços : MaterialSkin.Controls.MaterialForm
    {
        public int I;
        public ConsultaEndereços(int i)
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            I = i;
            if (I == 0)
            {
                btnAlterar.Enabled = false;
                btnExcluir.Enabled = false;
            }
        }
        private void Atualizar()
        {
            new Controller.EnderecoController().ListarEndereco(dgvEndereco);
        }
        private void ConsultaEnderecos_Load(object sender, EventArgs e)
        {
            Atualizar();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja excluir o Endereço?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                if (new EnderecoController().Deletar(int.Parse(dgvEndereco.CurrentRow.Cells[0].Value.ToString())))
                {
                    MessageBox.Show("Endereço excluido com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Atualizar();
                }
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            AlteracaoEndereco alteracaoEndereco = new AlteracaoEndereco(dgvEndereco.CurrentRow.Cells[0].Value.ToString(), dgvEndereco.CurrentRow.Cells[2].Value.ToString(),
                dgvEndereco.CurrentRow.Cells[3].Value.ToString(), dgvEndereco.CurrentRow.Cells[4].Value.ToString(), dgvEndereco.CurrentRow.Cells[5].Value.ToString(), dgvEndereco.CurrentRow.Cells[6].Value.ToString(), dgvEndereco.CurrentRow.Cells[1].Value.ToString());
            alteracaoEndereco.ShowDialog();
        }
    }
}
