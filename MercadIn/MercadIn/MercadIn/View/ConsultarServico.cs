﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class ConsultarServico : MaterialSkin.Controls.MaterialForm
    {
        public int I;
        public ConsultarServico(int i)
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            I = i;
            if (I == 0)
            {
                btnAlterar.Enabled = false;
                btnExcluir.Enabled = false;
            }
        }

        private void ConsultarServico_Load(object sender, EventArgs e)
        {
            Atualizar();
        }
        public void Atualizar()
        {
            new Controller.ServicoController().ListarServicos(dgvServicos);
        }
        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            AlteracaoServicos alteracaoRemocaoServicos = new AlteracaoServicos(dgvServicos.CurrentRow.Cells[1].Value.ToString(), dgvServicos.CurrentRow.Cells[0].Value.ToString(), dgvServicos.CurrentRow.Cells[2].Value.ToString(), decimal.Parse(dgvServicos.CurrentRow.Cells[3].Value.ToString()));
            alteracaoRemocaoServicos.ShowDialog();
            Atualizar();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja excluir o serviço?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                if (new ServicoController().Deletar(int.Parse(dgvServicos.CurrentRow.Cells[0].Value.ToString()))){
                    MessageBox.Show("Serviço excluido com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Atualizar();
                }
            }

        }
    }
}
