﻿using System.Configuration;
using MySql.Data.MySqlClient;

namespace MercadIn.Conexao
{
    class Conexao
    {
        private string connectionString;

        public string getConnectionString()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MercadLn.Properties.Settings.crudConnectionString"].ConnectionString;

            return connectionString;

        }

    }
}