﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using MercadIn.Conexao;

namespace MercadIn.Model
{
    class Servicos
    {
            public int id { get; set; }

            public string descricao { get; set; }

            public decimal preco { get; set; }

            public string nome { get; set; }

        public bool Crud(Servicos serv)
            {
            string strSQL;
            if (serv.id == 0)
            {
                strSQL = "insert into vini.Servicos (nome, descricao, preco) values (";
                strSQL += "'" + serv.nome + "',";
                strSQL += "'" + serv.descricao + "',";
                strSQL += "'" + serv.preco + "')";               
                return new DB().Insert(strSQL);
             }
            else
             {
                strSQL = "update vini.Servicos set nome = '" + serv.nome + "',descricao = '" + serv.descricao + "',preco = '" + serv.preco + "' where id = " + serv.id;
                return new DB().Update(strSQL);
             }

        }     
         public DataTable ListarServicos()
        {
            return new DB().Select("select id, nome, descricao, preco from vini.Servicos");
        }

        public Servicos ConsultarServicos(int id)
        {
            return new DB().ConsultarServico(id);
        }
        public bool Deletar(int id)
        {
            return new DB().Delete("delete from vini.Servicos WHERE id = " + id);
        }
    }
}
