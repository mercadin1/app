﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using DGVPrinterHelper;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class ConsultarCliente : MaterialSkin.Controls.MaterialForm
    {
        public int I;
        public ConsultarCliente(int i)
        {            
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            I = i;
            if (I == 0)
            {
                btnAlterar.Enabled = false;
                btnExcluir.Enabled = false;
                materialFlatButton1.Enabled = false;
            }
        }

        public void Atualizar()
        {
            new Controller.ClienteController().Listar(dgvClientes);
        }
        private void ConsultarCliente_Load(object sender, EventArgs e)
        {
            Atualizar();
          
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {           
            Hide();          
            AlteracaoCliente alteracaoRemocaoCliente = new AlteracaoCliente(dgvClientes.CurrentRow.Cells[0].Value.ToString(), dgvClientes.CurrentRow.Cells[1].Value.ToString(), dgvClientes.CurrentRow.Cells[2].Value.ToString(), dgvClientes.CurrentRow.Cells[3].Value.ToString(), dgvClientes.CurrentRow.Cells[4].Value.ToString(), dgvClientes.CurrentRow.Cells[5].Value.ToString());
            alteracaoRemocaoCliente.ShowDialog();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja excluir o Cliente?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                
                if (new ClienteController().Deletar(int.Parse(dgvClientes.CurrentRow.Cells[0].Value.ToString()))){
                    MessageBox.Show("Cliente excluido com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Atualizar();
                }
            }
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            DGVPrinter print = new DGVPrinter();           
            print.Title = "RELATÓRIO DE CLIENTES\n\n\n";
            print.SubTitleAlignment = StringAlignment.Near;
            print.SubTitle = string.Format("DATA: {0}", DateTime.Now.Date.ToString("dd/MM/yyyy"));
            print.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
            print.PageNumbers = true;
            print.PageNumberInHeader = false;
            print.PorportionalColumns = true;            
            print.HeaderCellAlignment = StringAlignment.Center;
            print.Footer = "Mercadin";
            print.FooterSpacing = 15;           
            print.PrintDataGridView(dgvClientes);           
        }
    }
}
