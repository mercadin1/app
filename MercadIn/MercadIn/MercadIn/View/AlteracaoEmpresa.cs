﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class AlteracaoEmpresa : MaterialSkin.Controls.MaterialForm // Atribuindo Design do MaterialSkin no form.
    {
        // Vars para receber do ConsultarEmpresa
        public string ID;
        public string NOME;
        public string RAZAOSOCIAL;
        public string CNPJ;

        public AlteracaoEmpresa(string id, string nome, string razaoSocial, string cnpj)
        {
            InitializeComponent();
            // Definindo design com bibliotécas do Material Skin
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            // Setando os valores nas variaveis 
            ID = id;
            NOME = nome;
            RAZAOSOCIAL = razaoSocial;
            CNPJ = cnpj;
        }

        private void AlteracaoRemocaoEmpresa_Load(object sender, EventArgs e)
        {
            // Atribuindo os valores que estão nas vars, para os campos de textos.
            txtID.Text = ID;
            txtNomeEmpresa.Text = NOME;
            txtRazaoEmpresa.Text = RAZAOSOCIAL;
            maskedTextBox1.Text = CNPJ;
        }
        public void LimparCampos() // Método para limpar os campos
        {
            txtNomeEmpresa.Text = "";
            txtRazaoEmpresa.Text = "";
            maskedTextBox1.Text = "";
        }


        private void btnVoltar_Click(object sender, EventArgs e)
        {
            // Evento de fechar tela.
            this.Close();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            // Mandando os valores para EmpresaController, no método EmpresaCrud caso de certo na alteração ira aparecer um
            // MessageBox, depois limpar os campos, fechando a tela e abrindo a ConsultarEmpresa
            // Se não aparece um messagebox com erro.
            if (new EmpresaController().EmpresaCrud(int.Parse(txtID.Text), txtNomeEmpresa.Text, txtRazaoEmpresa.Text, maskedTextBox1.Text))
            {
                MessageBox.Show("Empresa alterado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LimparCampos();
                Hide();
                ConsultarEmpresa cm = new ConsultarEmpresa(1);
                cm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Erro ao alterar a empresa!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimparCampos(); // Puxando método para limpar os campos
        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }
    }
}
