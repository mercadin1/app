﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MercadIn.Model;
using MercadIn.View;
using System.Windows.Forms;
using MercadIn.Conexao;

namespace MercadIn.Controller
{
    class EmpresaController
    {
        public bool EmpresaCrud(int id, string nome, string razaosocial, string cnpj)
        {
            Empresa empresa = new Empresa();
            empresa.id = id;
            empresa.nome = nome;
            empresa.razaoSocial = razaosocial;
            empresa.cnpj = cnpj;

            return new Empresa().CrudEmpresa(empresa);
        }

        public void ListarEmpresas(DataGridView objetoResposta)
        {
            objetoResposta.DataSource = new Empresa().ListarEmpresas();
        }
        public bool Deletar(int id)
        {
            return new Empresa().Deletar(id);
        }
    }
}
