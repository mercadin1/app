﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MercadIn.View
{
    public partial class MenuFuncionario : Form
    {
        public MenuFuncionario()
        {
            InitializeComponent();
        }

        private void categoriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarCategoria consultaCategoria = new ConsultarCategoria(0);
            consultaCategoria.ShowDialog();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarCliente ConsultarCliente = new ConsultarCliente(0);
            ConsultarCliente.ShowDialog();
        }

        private void produtosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarProdutoss consultarProdutoss = new ConsultarProdutoss(0);
            consultarProdutoss.ShowDialog();
        }

        private void servicosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarServico consultarServico = new ConsultarServico(0);
            consultarServico.ShowDialog();
        }

        private void empresasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarEmpresa consultarEmpresa = new ConsultarEmpresa(0);
            consultarEmpresa.ShowDialog();
        }

        private void endereçosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultaEndereços consulta = new ConsultaEndereços(0);
            consulta.ShowDialog();
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login lg = new Login();
            lg.ShowDialog();
        }

        private void caixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Caixa cx = new Caixa();
            cx.ShowDialog();
        }
    }
}
