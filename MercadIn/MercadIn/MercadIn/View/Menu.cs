﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DGVPrinterHelper;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;

namespace MercadIn.View
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();         
        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login lg = new Login();
            lg.ShowDialog();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarCliente ConsultarCliente = new ConsultarCliente(1);
            ConsultarCliente.ShowDialog();
        }

        private void cadastroDeClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroDeCliente cadastrocli = new CadastroDeCliente();
            cadastrocli.ShowDialog();
        }

        private void cadastroDeServiçosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroDeServicos cadastroDeServicos = new CadastroDeServicos();
            cadastroDeServicos.ShowDialog();
        }

        private void servicosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarServico consultarServico = new ConsultarServico(1);
            consultarServico.ShowDialog();
        }

        private void cadastroDeEmpresaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroDeEmpresa cadastroEmpresa = new CadastroDeEmpresa();
            cadastroEmpresa.ShowDialog();
        }

        private void empresasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarEmpresa consultarEmpresa = new ConsultarEmpresa(1);
            consultarEmpresa.ShowDialog();
        }

        private void caixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Caixa caixa = new Caixa();
            caixa.ShowDialog();
        }

        private void cadastroDeProdutosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroDeProdutos cadastroDeProdutos = new CadastroDeProdutos();
            cadastroDeProdutos.ShowDialog();
        }

        private void produtosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarProdutoss consultarProdutoss = new ConsultarProdutoss(1);
            consultarProdutoss.ShowDialog();
        }

        private void cadastroDeCategoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroDeCategoria cadastroDeCategoria = new CadastroDeCategoria();
            cadastroDeCategoria.ShowDialog();
        }

        private void categoriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarCategoria consultaCategoria = new ConsultarCategoria(1);
            consultaCategoria.ShowDialog();
        }

        private void produtosDisponíveisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EstoqueDeProdutos estoqueProdutos = new EstoqueDeProdutos();
            estoqueProdutos.ShowDialog();
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void produtosAVencerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProdutoVencer produtoVencer = new ProdutoVencer();
            produtoVencer.ShowDialog();
        }

        private void comprasEQuitaçõesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetarIdDoCliente id = new SetarIdDoCliente();
            id.ShowDialog();
        }

        private void cadastroDeEndereçoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroDeEndereco cadastro = new CadastroDeEndereco();
            cadastro.ShowDialog();
        }

        private void endereçosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultaEndereços consulta = new ConsultaEndereços(1);
            consulta.ShowDialog();
        }

        private void fatuasEmAtrasoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ID id = new ID();
            id.ShowDialog();
        }

        private void quitaçõesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FaturaID fatura = new FaturaID();
            fatura.ShowDialog();
        }

        private void quitarFaturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ID id = new ID();
            id.ShowDialog();
        }
    }
}
