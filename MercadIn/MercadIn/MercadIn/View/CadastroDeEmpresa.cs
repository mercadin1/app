﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class CadastroDeEmpresa : MaterialSkin.Controls.MaterialForm // Atribuindo Design do MaterialSkin no form.
    {
        public CadastroDeEmpresa()
        {
            InitializeComponent();
            // Definindo design com bibliotécas do Material Skin
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
        }

        private void CadastroEmpresa_Load(object sender, EventArgs e)
        {

        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            // Verificação se todos os campos estiverem com dados setados para poder continuar se os campos não estiverem completos ira.
            // Exibir uma mensagem "Preencha os campos obrigatórios!".
            if (txtNomeEmpresa.Text != "" && maskedTextBox1.Text != "" && txtRazaoEmpresa.Text != "")
            {
                // Enviando todos os dados necessários para o EmpresaController para executar o método EmpresaCrud se estiver tudo ok ele ira retornar um
                // MessageBox, e limpar os campos se não ira apresentar um MessageBox de erro.
                if (new EmpresaController().EmpresaCrud(0, txtNomeEmpresa.Text, txtRazaoEmpresa.Text, maskedTextBox1.Text))
                {
                    MessageBox.Show("Empresa adicionado com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LimparCampos();
                }
                else
                {
                    MessageBox.Show("Erro ao adicionar a empresa", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Preencha os campos obrigatórios!", "ERRO!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close(); // Fechando a view.
        }

        public void LimparCampos() // Método Limpar os Campos
        {
            txtNomeEmpresa.Text = "";
            maskedTextBox1.Text = "";
            txtRazaoEmpresa.Text = "";
        }
        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimparCampos(); // Puxando o método de limpar os campos.
        }

        private void txtCNPJ_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se a tecla digitada não for número
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                //Atribui True no Handled para cancelar o evento
                e.Handled = true;
            }
        }
    }
 }