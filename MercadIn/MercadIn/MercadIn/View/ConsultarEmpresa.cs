﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MercadIn.Model;
using MercadIn.Controller;

namespace MercadIn.View
{
    public partial class ConsultarEmpresa : MaterialSkin.Controls.MaterialForm
    {
        public int I;
        public ConsultarEmpresa(int i)
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
            Primary.Blue400, Primary.LightBlue700,
            Primary.Blue500, Accent.LightBlue700,
            TextShade.WHITE);
            I = i;
            if (I == 0)
            {
                btnAlterar.Enabled = false;
                btnExcluir.Enabled = false;
            }
        }
        private void Atualizar()
        {
            new Controller.EmpresaController().ListarEmpresas(dgvEmpresa);
        }
        private void ConsultarEmpresa_Load(object sender, EventArgs e)
        {
            Atualizar();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {           
            AlteracaoEmpresa alteracaoRemocaoEmpresa = new AlteracaoEmpresa(dgvEmpresa.CurrentRow.Cells[0].Value.ToString(), dgvEmpresa.CurrentRow.Cells[1].Value.ToString(), dgvEmpresa.CurrentRow.Cells[2].Value.ToString(), dgvEmpresa.CurrentRow.Cells[3].Value.ToString());
            alteracaoRemocaoEmpresa.ShowDialog();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja excluir a Empresa?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                if (new EmpresaController().Deletar(int.Parse(dgvEmpresa.CurrentRow.Cells[0].Value.ToString())))
                {
                    MessageBox.Show("Empresa excluido com sucesso!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Atualizar();
                }

            }
        }
    }
}
