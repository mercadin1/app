﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MercadIn.Conexao;


namespace MercadIn.Model
{
    class Produto
    {
        public int id { get; set; }

        public string dataProduto { get; set; }

        public int quantidade { get; set; }

        public decimal precoVenda { get; set; }

        public decimal precoCompra { get; set; }

        public string nome { get; set; }

        public string codigoBarras { get; set; }

        public int idCategoria { get; set; }

        public int idEmpresa { get; set; }

        public bool CrudProduto(Produto prod)
        {
            string strSQL;
            if (prod.id == 0)
            {
                strSQL = "insert into vini.Produtos (dataProduto, quantidade, precoVenda, precoCompra, nome, codigoBarras, idCategoria, idEmpresa) values (";
                strSQL += "'" + prod.dataProduto + "',";
                strSQL += +prod.quantidade + ",";
                strSQL += "'" +prod.precoVenda + "',";
                strSQL += "'" +prod.precoCompra +  "',";
                strSQL += "'" + prod.nome + "',";
                strSQL += "'" + prod.codigoBarras + "',";
                strSQL += +prod.idCategoria + ",";
                strSQL += +prod.idEmpresa + ")";
                return new DB().Insert(strSQL);
            }
            else {
                strSQL = "update vini.Produtos set dataProduto = '" + prod.dataProduto + "', quantidade =  " + prod.quantidade + ",precoVenda = '" + prod.precoVenda + "', precoCompra = '" + prod.precoCompra + "', nome = '" + prod.nome + "', codigoBarras = '" + prod.codigoBarras + "' where id = " + prod.id;
                return new DB().Update(strSQL);
                }
        }
        public DataTable ListarProdutos()
        {
            return new DB().Select("select id, nome, dataProduto, quantidade, precoVenda, precoCompra, codigoBarras from vini.Produtos");
        }
        public DataTable ListarProduto()
        {
            return new DB().Select("SELECT id, nome FROM vini.Produtos");
        }
        public Produto ConsultarProduto(int id)
        {
            return new DB().ConsultarProduto(id);
        }
        public bool Deletar(int id)
        {
            return new DB().Delete("delete from vini.Produtos where id =" + id);
        }
        public DataTable ListarEstoque()
        {
            return new DB().Select("SELECT id, nome, quantidade estoque, precoCompra, precoVenda, (quantidade * precoCompra) valorPago, (quantidade * precoVenda) valorArrecadado  FROM vini.Produtos");
        }
        public DataTable ListarProdutosVencer()
        {
            return new DB().Select("SELECT id, nome, dataProduto, quantidade, precoCompra, precoVenda, codigoBarras FROM vini.Produtos WHERE str_to_date(dataProduto, '%d/%m/%Y') < date_add(current_date(), interval +30 day)");
        }
    }
}
